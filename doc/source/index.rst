.. Analysis_tool documentation master file, created by
   sphinx-quickstart on Sun Nov 27 20:00:16 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Analysis_tool's documentation!
=========================================

.. Contents:

=====================
General description
=====================

.. toctree::
   :maxdepth: 4

   README.md

=============
STM Package
=============

.. toctree::
   :maxdepth: 2

RHK
---------

.. automodule:: stm.rhksm4
    :members:
    :undoc-members:
    :show-inheritance:

QPI
-------

.. automodule:: stm.qpi
    :members:
    :undoc-members:
    :show-inheritance:

---------------------

========================
Pulselaser Package
========================

Module contents
---------------

.. automodule:: pulselaser
   :members:
   :undoc-members:
   :show-inheritance:

pulselaser.berek module
-----------------------

.. automodule:: pulselaser.berek
   :members:
   :undoc-members:
   :show-inheritance:

pulselaser.nlo module
---------------------

.. automodule:: pulselaser.nlo
   :members:
   :undoc-members:
   :show-inheritance:

pulselaser.sellmeier module
---------------------------

.. automodule:: pulselaser.sellmeier
   :members:
   :undoc-members:
   :show-inheritance:


pulselaser.bloch module
---------------------------

.. automodule:: pulselaser.bloch
   :members:
   :undoc-members:
   :show-inheritance:



------------------

=================
PES Package
=================

The following Two modules are obsolute.

* pes.splab: Load SPecsLab2 data  
* pes.arpes: Analyze ARPES data (Angle-Energy-intensity)

.. automodule:: pes.itx
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: pes.lpes_basic
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: pes.calib1d
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: pes.prodigy_util
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: pes.splab
    :members:
    :undoc-members:
    :show-inheritance:


.. automodule:: pes.arpes
    :members:
    :undoc-members:
    :show-inheritance:


----------------------------

===============
SC Package
===============

Library for calculating superconductor properties.

.. automodule:: sc.bcs
   :members:
   :undoc-members:
   :show-inheritance:



--------------------------------

=================
EBAND Package
=================


.. automodule:: eband.tb_hex
    :members:
    :undoc-members:
    :show-inheritance:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
