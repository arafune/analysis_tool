"""Module for generation of the Brillouin zone."""

import numpy as np
from numpy.typing import NDArray
from scipy.spatial import Voronoi


def get_bz_3d(
    input_cell: NDArray[np.float64],
) -> tuple[NDArray[np.float64], list[NDArray[np.float64]], list[NDArray[np.float64]]]:
    """Generate the Brillouin Zone of a given cell.

    The BZ is the Wigner-Seitz cell of the reciprocal lattice,
    which can be constructed by Voronoi decomposition
    to the reciprocal lattice.
    A Voronoi diagram is a subdivision of the space
    into the nearest neighborhoods of a given set of points.

    https://en.wikipedia.org/wiki/Wigner%E2%80%93Seitz_cell
    https://docs.scipy.org/doc/scipy/reference/tutorial/spatial.html#voronoi-diagrams

    Parameters
    ----------
    input_cell
        [TODO:description]

    """
    cell: NDArray[np.float64] = np.asarray(input_cell, dtype=float)
    assert cell.shape == (3, 3)

    points_ = np.tensordot(cell, np.mgrid[-1:2, -1:2, -1:2], axes=[0, 0])
    px, py, pz = points_[0], points_[1], points_[2]
    points = np.c_[px.ravel(), py.ravel(), pz.ravel()]

    vor = Voronoi(points)

    bz_facets = []
    bz_ridges = []
    bz_vertices = []
    for pid, rid in zip(vor.ridge_points, vor.ridge_vertices, strict=True):
        # WHY 13 ????
        # The Voronoi ridges/facets are perpendicular to the lines drawn between the
        # input points. The 14th input point is [0, 0, 0].
        if pid[0] == 13 or pid[1] == 13:  # noqa: PLR2004
            bz_ridges.append(vor.vertices[np.r_[rid, [rid[0]]]])
            bz_facets.append(vor.vertices[rid])
            bz_vertices += rid

    bz_vertices = list(set(bz_vertices))

    return vor.vertices[bz_vertices], bz_ridges, bz_facets
