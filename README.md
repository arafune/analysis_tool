# Analys_tool #

Python scripts for analyze the experimental data.  At present, the following 3 Packages are provided.

## stm

  For analyzing STM (RHK) data. It contains two modules.

    * qpi
      * Analyze QPI data.
    * rhksm4
      * Load RHK (\*.sm4) file.

## pes

  For analyzing the ARPES (SpecsLab2) data.  It contains following two modules.

    * splab
    * arpes

## pulselaser

  At present, the sellmeier's equation for several laser materials

    * sellmeier


### What is this repository for? ###

* Quick summary
* Version: 0.0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Now preparing the setup script...
* Dependencies: numpy, matplotlib
* Tests: use nose

### Who do I talk to? ###

* Just myself only.
