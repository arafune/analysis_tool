# pulselaser module

自分のため。

## Active Library

- **init**.py: GDD や sech^2, gaussian_pulse などの基本関数。
- berek.py: Berek wave plate の評価用関数。
- nlo.py: 非線形結晶のcutting angle を決めるときに。
- sellmeire.py: 様々な物質の 屈折率分散（Sellmeier 係数）
- bloch.py: ２準位系のブロッホ方程式.

## Todo
