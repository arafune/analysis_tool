#!/usr/bin/env python3
"""Module for analyzing the CePc2, TbPc2 on Au(111) vasp calculation
Code for vasp calculation results requested by Prof. Komeda (Nov, 2020)
(1x1) lattice
"""

DIFF_FROM_WHOLE_INDEX: dict[str, int] = {
    "C": -1,
    "H": 63,
    "N": 95,
    "Ce": 111,
    "Au": 112,
}  # 基本的には使わない。

molecule: dict[str, list[int]] = {
    "C_low": list(range(32)),
    "C_up": list(range(32, 64)),
    "H_low": list(range(64, 80)),
    "H_up": list(range(80, 96)),
    "N_low": list(range(96, 104)),
    "N_up": list(range(104, 112)),
    "Ce": [112],
    "Tb": [112],
    "series": list(range(113)),
}

if __name__ == "__main__":
    pass
